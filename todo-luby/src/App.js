import React from "react";
import styled from "styled-components";
import * as yup from "yup";
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

const Container = styled.div`
    display:flex;
    flex-direction:row;
  `;

  const Form = styled.div`
    display:flex;
    height:100vh;
    width:30%;
    flex-direction:column;
  `;

  const TodoList = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    height: 100vh;
    width: 50%;
    flex-direction: column;
  `;

  const List = styled.div`
    display: flex;
    flex-direction: column;
    border: 2px black solid;
    border-radius: 10px;
    box-shadow: 5px 10px 5px #ccc;
    width: 80%;
    height: 80%;
    overflow-y: scroll;
    padding: 30px;
  `;

  const InputTask = styled.input`
    border: 2px solid black;
    border-radius: 10px;
    height: 40px;
    width: 400px;
    margin-left:20px;
    padding-left:10px;
    box-shadow: 5px 5px 2px #ccc;
    font-size: 17px;
    font-family: Fantasy;
  `;
  const Title = styled.p`
    font-family: Fantasy;
    font-size: 30px;
    font-weight: bold;
    margin: 100px 0px 20px 20px;
  `;

  const Button = styled.button`
    height: 40px;
    width: 100px;
    border-radius: 10px;
    margin: 20px;
    color: white;
    background-color: ${props => props.disabled ? "#575e5c" : "#145996"};
    font-family: Fantasy;
    font-weight: bold;
    box-shadow: 5px 5px 2px #ccc;
    cursor: pointer;
  `;

  const Task = styled.li`
    display: flex;
    flex-direction: row;
    align-items: center;
    font-family: Fantasy;
    font-size: 25px;
    font-weight: bold;
    margin: 10px 0px 10px 0px;
    text-decoration: ${props => props.checked ? "line-through" : null};
  `;

  const TitleList = styled.p`
    font-size: 40px;
    font-weight: bold;
    font-family: Fantasy;
    margin: 10px 0px 10px 0px;
  `;

const App = () => {
  const [task, setTask] = React.useState("");
  const [flag, setFlag] = React.useState(false);
  const schema = yup.string().test("len","Must have 5 or more characters", val => val.length >= 5);
  
  const putTask = () => {
    const data = JSON.parse(localStorage.getItem("Todo")) || [];
    data.push({"task":task, "status":false});
    localStorage.setItem("Todo", JSON.stringify(data));
    setTask("");
  };

  const getList = () => {
    return JSON.parse(localStorage.getItem("Todo")) || [];
  };

  const orderList = () => {
    const list = JSON.parse(localStorage.getItem("Todo")) || null;
    if(list != null){
      localStorage.setItem("Todo", JSON.stringify(list.sort(function(a, b){
          if(a.task.toLowerCase() < b.task.toLowerCase()) { return -1; }
          if(a.task.toLowerCase() > b.task.toLowerCase()) { return 1; }
          return 0;
      })));
      setFlag(!flag);
    }
  };
  
  const clearTasks = () => {
    localStorage.removeItem("Todo");
    setFlag(!flag);
  };
  
  const changeTaskStatus = (index) => {
    const data = JSON.parse(localStorage.getItem("Todo")) || null;
    console.log(data[index].status)
    console.log(index)
    
    if(data != null){
      console.log("entrei")
      data[index].status = !data[index].status
      localStorage.setItem("Todo", JSON.stringify(data))
      setFlag(!flag)
    }
  };

  return(
    <Container>
      <Form>
        <Title>Make a To Do List here!</Title>
        <InputTask 
          type="text" 
          placeholder="Put a task" 
          value={task} 
          onChange={e => setTask(e.target.value)} />
        <Button 
          disabled={!schema.isValidSync(task)} 
          onClick={putTask} >
            Add task
        </Button>
        <Button onClick={orderList} >
          Order Tasks
        </Button>
        <Button onClick={clearTasks} >
          Clear Tasks
        </Button>
      </Form>
      <TodoList>
        <List>
          <TitleList>To Do List</TitleList>
          <ol>
            {getList().map((item,i) => (
              <Task key={i} checked={item.status}>
                {(i+1)+". "}
                {item.task}
                <div key={i} onClick={changeTaskStatus.bind(this, i)} data-key={i}>
                  {item.status ?
                    <CheckBoxIcon style={{marginLeft:5}} />
                    :
                    <CheckBoxOutlineBlankIcon style={{marginLeft:5}} />
                  }
                </div>
              </Task>
              
            ))}
          </ol>
        </List>
      </TodoList>
    </Container>
  )
}
export default App;
